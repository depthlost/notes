
import json

def read_json_file(filename):
    with open(filename, "r") as file:
        content = json.load(file)
    
    return content

def write_json_file(filename, content):
    with open(filename, "w") as file:
        file.write(json.dumps(content, indent=4))

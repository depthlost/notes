# Notes

Simple command line interface system for note management.

## Usage

```
usage: run.py [-h] [--config-file filename] [--set config value] [--list]
              [--new] [--new-directory]
              [path]

positional arguments:
  path                  path to be used

optional arguments:
  -h, --help            show this help message and exit
  --config-file filename
                        file used to save settings (default: config.json)
  --set config value, -s config value
                        update the configuration file
  --list, -l            list files in the specified directory
  --new, -n             create a new note
  --new-directory, -d   create a new directory
```
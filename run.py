#!/usr/bin/python3

import os
import sys
import argument_parser

from config import Config

arguments = argument_parser.parse()

if not os.path.isfile(arguments.config_file):
    print("the configuration file does not exist.")
    print(f"a new configuration file will be created in \"{arguments.config_file}\"")

    file_created = Config.create(arguments.config_file)

    if not file_created:
        sys.exit("error: the configuration file could not be created")

config = Config.get(arguments.config_file)

if not config:
    sys.exit("error: the configuration file could not be loaded")

if not config.validate():
    sys.exit("error: invalid configuration")

if arguments.set_config:
    if arguments.set_config[0] not in ("directory", "editor"):
        sys.exit(f"error: invalid option \"{arguments.set_config[0]}\"")

    config.__dict__[arguments.set_config[0]] = arguments.set_config[1]
    file_saved = config.save()

    if not file_saved:
        sys.exit("error: the configuration file could not be updated")

    sys.exit()

if arguments.list_files:
    path = os.path.join(config.directory, arguments.path or "")
    
    if not os.access(path, os.R_OK):
        sys.exit(f"error: invalid path \"{path}\"")
    
    os.system(f"ls --color=auto \"{path}\"")
    sys.exit()

if not arguments.path:
    sys.exit("error: a path is required")

path = os.path.join(config.directory, arguments.path)

if arguments.new_directory:
    try:
        os.makedirs(path)
    except:
        sys.exit(f'error: could not create directory "{path}"')

    sys.exit()

if arguments.new_file:
    try:
        open(path, 'x').close()
    except:
        sys.exit(f'error: could not create file "{path}"')
else: 
    if os.path.isdir(path):
        sys.exit(f'error: path "{path}" is a directory')
    
    if not os.access(path, os.W_OK):
        sys.exit(f'error: cannot access "{path}" file')

os.system(f'{config.editor} "{path}"')


import argparse

def parse():
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--config-file", dest="config_file", type=str, help="file used to save settings (default: config.json)", metavar="filename", default="config.json", required=False)
    parser.add_argument("--set", "-s", dest="set_config", type=str, help="update the configuration file", metavar=("config", "value"), nargs=2, required=False)
    parser.add_argument("--list", "-l", dest="list_files", help="list files in the specified directory", action='store_true')
    parser.add_argument("--new", "-n", dest="new_file", help="create a new note", action='store_true')
    parser.add_argument("--new-directory", "-d", dest="new_directory", help="create a new directory", action='store_true')
    parser.add_argument("path", type=str, help="path to be used", metavar="path", nargs="?")

    return parser.parse_args()

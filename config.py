
import os

from file_utility import read_json_file, write_json_file

class Config:
    def __init__(self, directory, editor, filename):
        self.directory = directory
        self.editor = editor
        self.filename = filename

    def to_dictionary(self):
        return {
            "directory": self.directory,
            "editor": self.editor
        }

    def save(self):
        try:
            write_json_file(self.filename, self.to_dictionary())

            return True
        except:
            return False

    def validate(self):
        return os.access(self.directory, os.W_OK)

    @classmethod
    def get(self, filename):
        try:
            return Config(**read_json_file(filename), filename=filename)
        except:
            return None
    
    @classmethod
    def create(self, filename):
        directory = input("enter the root directory: ")
        editor = input("enter text editor: ")

        return Config(directory=directory, editor=editor, filename=filename).save()
